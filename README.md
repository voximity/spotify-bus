# spotify-bus

spotify-bus provides a CLI for interacting with a native Spotify client through DBus.

## Building

Build with `crystal build --release src/spotify-bus.cr`