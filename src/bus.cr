require "crystal-dbus"
require "json"
include DBus

module SBus
  SPOTIFY_DESTINATION = "org.mpris.MediaPlayer2.spotify"
  SPOTIFY_PATH = "/org/mpris/MediaPlayer2"
  SPOTIFY_PLAYER = "org.mpris.MediaPlayer2.Player"

  struct SpotifyMetadata
    JSON.mapping(
      track_id: {type: String, key: "mpris:trackid"},
      duration: {type: Int64, key: "mpris:length"},
      art_url: {type: String, key: "mpris:artUrl"},
      album: {type: String, key: "xesam:album"},
      album_artists: {type: Array(String), key: "xesam:albumArtist"},
      artists: {type: Array(String), key: "xesam:artist"},
      rating: {type: Float64, key: "xesam:autoRating"},
      title: {type: String, key: "xesam:title"},
      track_number: {type: Int16, key: "xesam:trackNumber"},
      url: {type: String, key: "xesam:url"}
    )
  end

  class Spotify
    getter bus : DBus::Bus
    getter destination : DBus::Object
    getter object : DBus::Object
    getter interface : DBus::Interface

    def initialize
      @bus = Bus.new
      @destination = @bus.destination(SPOTIFY_DESTINATION)
      @object = @destination.object(SPOTIFY_PATH)
      @interface = @object.interface(SPOTIFY_PLAYER)
    end

    def metadata
      SpotifyMetadata.from_json Variant.json @interface.get("Metadata").reply.first
    end

    def pause(force)
        @interface.call "#{!force ? "Play" : ""}Pause"
    end
  end
end