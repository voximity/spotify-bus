require "./bus"
require "cli"
require "colorize"
include SBus

class SpotifyBus < Cli::Supercommand
  command "metadata", default: true

  class Pause < Cli::Command
    class Help
      header "Pauses the currently playing song, or plays it if it is currently paused."
    end

    class Options
      bool "--force", desc: "forces song to pause, regardless of if it should play", default: false
      help
    end

    def run
      Spotify.new.pause args.force?
    end
  end

  class Metadata < Cli::Command
    class Help
      header "Writes out the metadata of the current Spotify context."
    end

    class Options
      bool "--json", desc: "write out JSON rather than prettified text", default: false
      help
    end

    def run
      meta = Spotify.new.metadata

      if args.json?
        puts meta.to_json
      else
        content = {
          meta.title => meta.artists.join(", "),
          "Album" => "#{meta.album} by #{meta.album_artists.join(", ")}",
          "Track Number" => meta.track_number.to_s
        }
        lengthy_key = content.keys.map(&.size).max
        lengthy_value = content.values.map(&.size).max
        i = 1
        content.each do |k, v|
          k_color = :light_red
          k_dec = :bright
          v_color = :red
          if i == 1
            k_color = :light_blue
            v_color = :blue
          end
          print k.colorize.fore(k_color).mode(k_dec)#k.ljust(lengthy_key + 3).colorize.fore(k_color).mode(k_dec)
          print ("." * (lengthy_key + 3 - k.size)).colorize.fore(:dark_gray)
          puts v.colorize.fore(v_color)
          if i == 1
            puts ("~" * (lengthy_key + 3 + lengthy_value)).colorize.fore(:white).mode(:bright)
          end
          i += 1
        end
      end
    end
  end
end

SpotifyBus.run ARGV